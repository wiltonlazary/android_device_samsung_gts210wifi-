# Release name
PRODUCT_RELEASE_NAME := gts210wifixx

# Inherit device configuration
$(call inherit-product, device/samsung/gts210wifixx/device.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := gts210wifixx
PRODUCT_NAME := omni_gts210wifixx
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-T810
PRODUCT_MANUFACTURER := samsung
